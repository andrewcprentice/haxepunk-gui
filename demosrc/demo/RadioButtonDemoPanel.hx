package demo;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.gui.ToggleButton;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import flash.events.Event;
import openfl.Assets;
import flash.geom.Point;
import flash.text.TextFormatAlign;

/**
 * ...
 * @author Lythom
 */

class RadioButtonDemoPanel extends DemoPanel
{
	
	private var buttons:Array<RadioButton> ;
	private var g1Label:Label;
	private var g2Label:Label;

	public function new(width:Int, height:Int)
	{
		super(0, 0, width, height, true);
		
		buttons = new Array<RadioButton>();
		
		var cursor:Point = new Point(10, 10);
		var margin:Int = 15;
		
		// place title
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		var l:Label = new Label("RadioButton Demo", cursor.x, cursor.y, Math.round(width - cursor.x));
		l.align = TextFormatAlign.CENTER;
		l.size = 16;
		l.color = 0x000000;
		addControl(l);
		
		// 1st group of radio
		RadioButton.defaultBoxSize = 12;
		cursor.y += margin + l.height;
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		for (i in 0...3)
		{
			var rb:RadioButton  = new RadioButton("group1", "Radio group1 value" + i, "value" + i, cursor.x, cursor.y, (i == 2) ? true : false);
			if (i == 0) rb.addEventListener(RadioButton.GROUP_CHANGED, buttonChanged);
			addControl(rb);
			rb.color = 0x120946;
			cursor.y += 5 + rb.height;
		}
		g1Label = new Label("value for group1 : " + RadioButton.getValue("group1"), cursor.x + 150, cursor.y - 50);
		addControl(g1Label);
		
		// 2nd group of radio
		RadioButton.defaultBoxSize = 0;
		cursor.y += margin;
		for (i in 0...3)
		{
			var rb:RadioButton  = new RadioButton("group2", "Radio group2 value" + i, "value" + i, cursor.x, cursor.y, (i == 2) ? true : false);
			if (i == 0) rb.addEventListener(RadioButton.GROUP_CHANGED, buttonChanged);
			addControl(rb);
			rb.color = 0x640000;
			cursor.y += 5 + rb.height;
		}
		g2Label = new Label("value for group2 : " + RadioButton.getValue("group1"), cursor.x + 150, cursor.y - 50);
		addControl(g2Label);
		
		
		cursor.y = globalInstructionLabel.y + globalInstructionLabel.height + margin;
		cursor.x = globalInstructionLabel.x;
		var instructions:String = "Specific Instructions :\n";
		instructions += "+ or P - increase text size\n";
		instructions += "- or M - decrease text size\n";
		instructions += "f - switch font family\n";
		instructions += "d - enable/disable buttons\n";
		l = new Label(instructions, cursor.x, cursor.y);
		addControl(l);
		
	}
	
	private function buttonChanged(e:Event):Void
	{
		g1Label.text = "value for group1 : " + RadioButton.getValue("group1");
		g2Label.text = "value for group2 : " + RadioButton.getValue("group2");
	}
	
	override public function addControl(child:Control, ?position:Int):Void
	{
		if (Std.is(child, RadioButton)) {
			buttons.push(cast(child,RadioButton));
		}
		super.addControl(child, position);
	}
	
	override public function update()
	{
		if (!enabled) {
			return;
		}
		
		if (Input.pressed(Key.NUMPAD_ADD) || Input.pressed(Key.P)) {
			for (b in buttons) {
				b.label.size++;
			}
		}
		if (Input.pressed(Key.NUMPAD_SUBTRACT) || Input.pressed(Key.M)) {
			for (b in buttons) {
				b.label.size--;
			}
		}
		if (Input.pressed(Key.F)) {
			for (b in buttons) {
				if (b.font == Assets.getFont("font/pf_ronda_seven.ttf").fontName) {
					b.font = Assets.getFont("font/04B_03__.ttf").fontName;
				} else {
					b.font = Assets.getFont("font/pf_ronda_seven.ttf").fontName;
				}
			}
		}
		if (Input.pressed(Key.D)) {
			for (b in buttons) {
				b.enabled = !b.enabled;
			}
		}
		super.update();
	}
	
}