import com.haxepunk.Engine;
import com.haxepunk.HXP;
import com.haxepunk.utils.Key;
import openfl.display.Stage;
import world.DemoScene;

class Main extends Engine
{
	override public function init()
	{
#if debug
	#if flash
		if (flash.system.Capabilities.isDebugger)
	#end
		{
			HXP.console.enable();
			HXP.console.toggleKey = Key.P;
		}
#end
		HXP.screen.scale = 2;
		HXP.resize(HXP.stage.stageWidth, HXP.stage.stageHeight);
		HXP.scene = new DemoScene();
	}

	public static function main()
	{
		new Main();
	}

}